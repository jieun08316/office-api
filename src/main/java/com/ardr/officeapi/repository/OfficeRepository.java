package com.ardr.officeapi.repository;


import com.ardr.officeapi.entity.Office;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficeRepository extends JpaRepository<Office, Long> {
}
