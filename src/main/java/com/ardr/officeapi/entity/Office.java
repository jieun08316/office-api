package com.ardr.officeapi.entity;


import com.ardr.officeapi.enums.OfficePosition;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Value;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    //id 필수
    @Column(nullable = false)
    private LocalDate joinDate;
    //입사일 필수
    @Column(nullable = false)
    private String employeeName;
    //이름
    @Column(nullable = false)
    private OfficePosition Position;
    //직급
    @Column(nullable = false)
    private String department;
    //부서
    @Column(nullable = false)
    private Boolean regularStatus;
    //정규 여부
    @Column(nullable = true)
    private String etcMemo;
    //비고
}
