package com.ardr.officeapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public enum OfficePosition {
    EMPLOYEE("사원"),
    FOREMEN("주임"),
    AN_ASSISTANT_MANAGER("대리"),
    EXAGGERATION("과장"),
    DEPUTY_DIRECTOR("차장"),
    HEAD_OF_DEPARTMENT("부장");

        private String name;
}
