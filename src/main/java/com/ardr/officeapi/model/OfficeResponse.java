package com.ardr.officeapi.model;

import com.ardr.officeapi.enums.OfficePosition;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OfficeResponse {
    private long id;
    private LocalDate joinDate;
    private String employeeName;
    private String PositionName;
    private String department;
    private String regularStatusName;
    private String etcMemo;
}
