package com.ardr.officeapi.model;

import com.ardr.officeapi.enums.OfficePosition;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OfficeRequest {
    private String employeeName;

    @Enumerated(value = EnumType.STRING)
    private OfficePosition Position;

    private String department;
    private Boolean regularStatus;
    private String etcMemo;

}

