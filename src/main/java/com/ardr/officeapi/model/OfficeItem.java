package com.ardr.officeapi.model;

import com.ardr.officeapi.enums.OfficePosition;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
public class OfficeItem {
    private long id;
    private LocalDate joinDate;
    private String employeeName;
    private OfficePosition Position;
    private String department;
    private Boolean regularStatus;
    private String etcMemo;
}
