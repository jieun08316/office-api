package com.ardr.officeapi.service;

import com.ardr.officeapi.controller.OfficeController;
import com.ardr.officeapi.entity.Office;
import com.ardr.officeapi.model.OfficeItem;
import com.ardr.officeapi.model.OfficeRequest;
import com.ardr.officeapi.model.OfficeResponse;
import com.ardr.officeapi.repository.OfficeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OfficeService {
    private final OfficeRepository officeRepository;

    public void setOffice(OfficeRequest request) {
        Office addData = new Office();
        addData.setJoinDate(LocalDate.now());
        addData.setEmployeeName(request.getEmployeeName());
        addData.setPosition(request.getPosition());
        addData.setDepartment(request.getDepartment());
        addData.setRegularStatus(request.getRegularStatus());
        addData.setEtcMemo(request.getEtcMemo());
        officeRepository.save(addData);
    }

    public List<OfficeItem> getOffices() {
        List<Office> originList = officeRepository.findAll();

        List<OfficeItem> result = new LinkedList<>();

        for (Office office : originList) {
            OfficeItem addItem = new OfficeItem();
            addItem.setId(office.getId());
            addItem.setEmployeeName(office.getEmployeeName());
            addItem.setPosition(office.getPosition());
            addItem.setDepartment(office.getDepartment());
            addItem.setRegularStatus(office.getRegularStatus());
            addItem.setEtcMemo(office.getEtcMemo());
            result.add(addItem);
        }
        return result;
    }

    public OfficeResponse getOffice(long id) {

        Office originData = officeRepository.findById(id).orElseThrow();

        OfficeResponse response = new OfficeResponse();

        response.setId(originData.getId());
        response.setJoinDate(originData.getJoinDate());
        response.setEmployeeName(originData.getEmployeeName());

        response.setPositionName(originData.getPosition().getName());

        response.setDepartment(originData.getDepartment());

        response.setRegularStatusName(originData.getRegularStatus() ? "정규" : "비정규");
        response.setEtcMemo(originData.getEtcMemo());


        return response;
    }

}






