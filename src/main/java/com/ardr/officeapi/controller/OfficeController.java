package com.ardr.officeapi.controller;

import com.ardr.officeapi.model.OfficeItem;
import com.ardr.officeapi.model.OfficeRequest;
import com.ardr.officeapi.model.OfficeResponse;
import com.ardr.officeapi.service.OfficeService;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/Office")
public class OfficeController {
    private final OfficeService officeService;

    @PostMapping("/people")
    public String setOffice(@RequestBody OfficeRequest request) {
        officeService.setOffice(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<OfficeItem> getOffices() {
        return officeService.getOffices();
    }

    @GetMapping("/detail/{id}")
    public OfficeResponse getOffice(@PathVariable long id){
        return officeService.getOffice(id);
    }
}
